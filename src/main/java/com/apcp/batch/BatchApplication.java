package com.apcp.batch;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication(
		exclude = {}
)
@EnableBatchProcessing
@EntityScan(
		basePackageClasses = {Jsr310JpaConverters.class},
		// java8 localdatetime 을 jpa에 적용하기위한 convert
		basePackages = {"com.apcp.batch"})
@EnableJpaAuditing
public class BatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(BatchApplication.class, args);
	}

}
