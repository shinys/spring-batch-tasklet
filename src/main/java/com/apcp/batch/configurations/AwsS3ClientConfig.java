package com.apcp.batch.configurations;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.*;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

@Configuration
@PropertySource(value = "classpath:application.yml")
public class AwsS3ClientConfig {

    @Value("${aws.s3.key.access}")
    private String accessKey;
    @Value("${aws.s3.key.secret}")
    private String secretKey;
    @Value("${aws.s3.region}")
    private String region;

    @Bean
    public AmazonS3 s3client(){

        AWSCredentials credentials = new BasicAWSCredentials(
                accessKey,
                secretKey
        );

        return AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.fromName(region))
                .withClientConfiguration(new ClientConfiguration()
                        .withConnectionTimeout(1000*8)
                        .withRequestTimeout(1000*60*4)
                        .withClientExecutionTimeout(1000*60*3)
                        .withConnectionMaxIdleMillis(1000*10)
                        .withCacheResponseMetadata(true)
                        .withGzip(true)
                        .withMaxConnections(20)
                        .withUserAgentPrefix("ecp-batch-server")
                )
                .build();

    }
}
