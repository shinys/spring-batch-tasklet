package com.apcp.batch.configurations;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.springframework.context.annotation.*;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.DefaultUriBuilderFactory;
import reactor.netty.http.client.HttpClient;
import reactor.netty.tcp.TcpClient;

import java.util.concurrent.TimeUnit;

@Configuration
public class WebClientConfig {

    @Primary
    @Bean
    public WebClient webClient(){

        TcpClient tcpClient = TcpClient
                .create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 80000)
                //.option(ChannelOption.SO_TIMEOUT, 4000)
                .option(ChannelOption.SO_KEEPALIVE, false)
                .option(ChannelOption.SO_REUSEADDR, true)
                .option(ChannelOption.TCP_NODELAY, true)
                // while we can call timeout as well on our client request, this is a signal timeout, not an HTTP connection or read/write timeout.
                .doOnConnected(connection -> {
                    connection.addHandlerLast(new ReadTimeoutHandler(40000, TimeUnit.MILLISECONDS));
                    connection.addHandlerLast(new WriteTimeoutHandler(40000, TimeUnit.MILLISECONDS));
                });

        DefaultUriBuilderFactory factory = new DefaultUriBuilderFactory();
        factory.setEncodingMode(DefaultUriBuilderFactory.EncodingMode.URI_COMPONENT);

        return WebClient.builder()
                //https://www.baeldung.com/webflux-webclient-parameters#encoding-mode
                .uriBuilderFactory(factory)
                .clientConnector(
                        new ReactorClientHttpConnector(HttpClient.from(tcpClient))
                ).build();
    }
}
