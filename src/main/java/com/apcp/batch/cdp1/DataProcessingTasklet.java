package com.apcp.batch.cdp1;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.io.File;

/**
 * 집계되어 S3에 저장된 파일을 다운로드하여 DB에 저장하는 tasklet
 */
@Log4j2
@RequiredArgsConstructor
public class DataProcessingTasklet implements Tasklet, StepExecutionListener {
    private final AmazonS3 s3client;

    @SneakyThrows
    @Override
    public void beforeStep(StepExecution stepExecution) {
        log.info("csv 파일 다운로드 하고");
//        String csvPath="";
//        S3Object s3object = s3client.getObject("BUCKET_NAME", csvPath.startsWith("/")?csvPath.substring(1):csvPath);
//        //FileUtils.copyInputStreamToFile(s3object.getObjectContent(), new File(tmpSavePath));
//        s3object.close();
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        return ExitStatus.COMPLETED;
    }

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

        log.info("csv 파일 데이터 DB에 저장~");

        log.info(contribution.getStepExecution().getJobExecution().getExecutionContext().get("key"));

        return RepeatStatus.FINISHED;
    }
}
