package com.apcp.batch.cdp1;

import com.amazonaws.services.s3.AmazonS3;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.listener.ExecutionContextPromotionListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class JobConfiguration {
    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final AmazonS3 s3client;

    @Bean
    public Job cdp1Job() {
        return jobBuilderFactory.get("cdp1Job")
                .start(aggregateRequestStep())
                .next(dataProcessingStep())
                .build();
    }

    @Bean
    public Step aggregateRequestStep() {
        return stepBuilderFactory.get("aggregateRequestStep")
                .tasklet(new AggregateRequestTasklet())
             //   .listener(promotionListener())
                .build();
    }

    @Bean
    public Step dataProcessingStep() {
        return stepBuilderFactory.get("dataProcessingStep")
                .tasklet(new DataProcessingTasklet(s3client))
              //  .listener(promotionListener())
                .build();
    }


//    //Step 간 ExecutionContext 를 통해 데이터 공유를 위한 리스너 bean 등록
//    @Bean
//    public ExecutionContextPromotionListener promotionListener () {
//        ExecutionContextPromotionListener executionContextPromotionListener = new ExecutionContextPromotionListener();
//        // 데이터 공유를 위해 사용될 key값을 미리 빈에 등록해주어야 합니다.
//        executionContextPromotionListener.setKeys(new String[]{"SPECIFIC_MEMBER","key"});
//
//        return executionContextPromotionListener;
//    }


}
