package com.apcp.batch.cdp1;

import lombok.extern.log4j.Log4j2;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 빅데이터플랫폼에 집계를 요청하는 tasklet
 */
@Log4j2
public class AggregateRequestTasklet implements Tasklet, StepExecutionListener {

    private static final AtomicInteger ai = new AtomicInteger();

    @Override
    public void beforeStep(StepExecution stepExecution) {
        log.info("자..  집계 요청 날리자..");
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {

        return ExitStatus.COMPLETED;
    }

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        int i = ai.incrementAndGet();
        log.info("csv 완성됐나?");


        if(i%10==0){
            log.info("오호 csv 만들어졌군... 오키 다음 step으로 넘어가자.");
            contribution.getStepExecution().getJobExecution().getExecutionContext().put("key","step 간 데이터 공유. step1에서 넣어 둠.");
            return RepeatStatus.FINISHED;


        }else {
            log.info("음.. 아직 안만들어 진겨? 좀 기다려보자.");
            return RepeatStatus.CONTINUABLE;
        }

    }
}
